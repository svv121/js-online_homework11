"use strict";
/*
*/
const passwordForm = document.querySelector('.password-form');
const password = document.querySelector('#password');
const repeatPassword = document.querySelector('#repeat_password');
const passwordInputs = passwordForm.querySelectorAll('input');
const checkPassword = () => {
    if(password.value.trim().length === 0){
        document.querySelector('.error').textContent = 'You should enter a valid password';
    }
    else if(repeatPassword.value !== password.value){
        document.querySelector('.error').textContent = 'The passwords you entered do not match';
    }
    else{alert(`You are welcome`)}
}
password.addEventListener('focus', () => {
    document.querySelector('.error').textContent = '';
});
repeatPassword.addEventListener('focus', () => {
    document.querySelector('.error').textContent = '';
});
passwordForm.addEventListener("click", event => {
    if (event.target !== event.currentTarget && event.target.classList.contains("fas")){
    if(event.target.classList.contains('fa-eye-slash')){
        event.target.classList.remove('fa-eye-slash');
        event.target.classList.add('fa-eye');
            event.target.previousElementSibling.type = 'password'
        }else{event.target.classList.remove('fa-eye');
        event.target.classList.add('fa-eye-slash');
            event.target.previousElementSibling.type = 'text'
        }
    }
})
const receiveValues = () => {
    const enteredValues = {};
    passwordInputs.forEach(input => {
        enteredValues[input.name] = input.value;
    })
    return enteredValues;
}
passwordForm.addEventListener('submit', (event) => {
    event.preventDefault();
    checkPassword();
    console.log(receiveValues());
    event.target.reset();
})